import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:newborn_care/models/register_baby_model.dart';
import 'package:newborn_care/screens/register_a_baby/components/toggle_buttons/gender_button.dart';
import 'package:newborn_care/screens/register_a_baby/components/toggle_buttons/skin_color_changes_button.dart';
import 'package:newborn_care/screens/register_a_baby/components/toggle_buttons/traumas_during_birth_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class RegisterBabyDetails extends StatefulWidget {
  final BabyDetailsModel _babyDetails;
  RegisterBabyDetails(this._babyDetails);

  @override
  _RegisterBabyDetailsState createState() => _RegisterBabyDetailsState();
}

class _RegisterBabyDetailsState extends State<RegisterBabyDetails> {
  TextEditingController weightcont = TextEditingController();
  late String? txt = "0";
  String dropdown = "grams";
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            AppLocalizations.of(context)!.babysDetails,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            AppLocalizations.of(context)!.gender,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ),
        GenderButton(AppLocalizations.of(context)!.male,
            AppLocalizations.of(context)!.female, widget._babyDetails),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            AppLocalizations.of(context)!.birthDateTime,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ),
        TextButton(
            onPressed: () {
              DatePicker.showDateTimePicker(context,
                  showTitleActions: true,
                  minTime:
                  DateTime(DateTime.now().year, DateTime.now().month, 1),
                  maxTime: DateTime.now(),
                  onChanged: (date) {}, onConfirm: (date) {
                    setState(() {
                      widget._babyDetails.birthDateTime = date;
                    });
                  }, locale: LocaleType.en);
            },
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(8)),
                              border: Border.all(color: Colors.blue)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(widget._babyDetails.birthDateTime.day
                                .toString()),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(8)),
                              border: Border.all(color: Colors.blue)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(widget._babyDetails.birthDateTime.month
                                .toString()),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(8)),
                              border: Border.all(color: Colors.blue)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(widget._babyDetails.birthDateTime.year
                                .toString()),
                          ),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(8)),
                              border: Border.all(color: Colors.blue)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(widget._babyDetails.birthDateTime.hour
                                .toString()),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(8)),
                              border: Border.all(color: Colors.blue)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(widget._babyDetails.birthDateTime.minute
                                .toString()),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            AppLocalizations.of(context)!.weightOfBaby,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ),
        // Center(child: WeightSlider(widget._babyDetails)),
        Center(child: Container(
          padding: EdgeInsets.only(left: 20,right: 20,top: 20),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  inputFormatters: [DecimalTextInputFormatter(decimalRange: 0)],
                  controller: weightcont,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.monitor_weight),
                    border: OutlineInputBorder(),
                  ),
                  keyboardType: TextInputType.numberWithOptions(decimal: true,signed: false),
                  onFieldSubmitted: (weightcont) {
                    setState(() {
                      if(dropdown == "grams") {
                        txt = weightcont;
                        widget._babyDetails.weight = double.parse(txt!);
                      }
                      else if(dropdown == "pounds") {
                        txt = (int.parse(weightcont)*453).toString();
                        widget._babyDetails.weight = double.parse(txt!);
                      }

                    });
                  },
                ),
                DropdownButton(
                  value: dropdown,
                  items: [DropdownMenuItem(child: Text("grams"), value: "grams",),
                    DropdownMenuItem(child: Text("pounds"),value: "pounds",),
                  ], onChanged: (String? value) {
                  setState(() {
                    dropdown = value!;
                  });
                },
                )
              ]
          ),
        ),),
        SizedBox(height: 20,),
        Center(child: Text("weight of the baby in grams is"+ widget._babyDetails.weight.toString()),),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            AppLocalizations.of(context)!.skinColorChangesInNewBorn,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ),
        SkinColorChangesButton(AppLocalizations.of(context)!.yes,
            AppLocalizations.of(context)!.no, widget._babyDetails),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            AppLocalizations.of(context)!.traumasDuringBirth,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ),
        TraumasDuringBirthButton(AppLocalizations.of(context)!.yes,
            AppLocalizations.of(context)!.no, widget._babyDetails),
      ]),
    );
  }
}

// class WeightSlider extends StatefulWidget {
//   final BabyDetailsModel _babyDetails;
//
//   WeightSlider(this._babyDetails);
//
//   @override
//   _WeightSliderState createState() => _WeightSliderState();
// }

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({required this.decimalRange})
      : assert(decimalRange == 0 || decimalRange > 0);

  final int decimalRange;

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue,
      TextEditingValue newValue,
      ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    String value = newValue.text;

    if (value.contains(".") &&
        value.substring(value.indexOf(".") + 1).length > decimalRange) {
      truncated = oldValue.text;
      newSelection = oldValue.selection;
    } else if (value == ".") {
      truncated = "0.";

      newSelection = newValue.selection.copyWith(
        baseOffset: truncated.length,
        extentOffset: truncated.length,
      );
    }

    return TextEditingValue(
      text: truncated,
      selection: newSelection,
      composing: TextRange.empty,
    );
  }
}

// class _WeightSliderState extends State<WeightSlider> {
//   int calculateNumber(int number) {
//     int a = number % 100;
//
//     if (a > 0) {
//       return (number ~/ 100) * 100 + 100;
//     }
//
//     return number;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return SfSliderTheme(
//       data: SfSliderThemeData(
//         activeMinorTickColor: Colors.red,
//         inactiveMinorTickColor: Colors.red[200],
//       ),
//       child: SfSlider(
//         min: 1000.0,
//         max: 4000.0,
//         interval: 1000,
//         showTicks: true,
//         showLabels: true,
//         enableTooltip: true,
//         minorTicksPerInterval: 5,
//         tooltipShape: SfPaddleTooltipShape(),
//         value: widget._babyDetails.weight,
//         onChanged: (dynamic newValue) {
//           setState(() {
//             double temp = newValue;
//             int pass = temp.toInt();
//             pass = calculateNumber(pass);
//             widget._babyDetails.weight = pass.toDouble();
//           });
//         },
//       ),
//     );
//   }
// }
